import React, { Component } from 'react';
import { Items } from './components/Items/Items';
import { connect } from 'react-redux'

class App extends Component {
  // state = {
  //   inputValue: '',
  //   items: []
  // };


  // changeValue = (event) => {
  //   this.setState({
  //     inputValue: event.target.value
  //   })
  //   console.log(this.state.inputValue, 'inputValue')
  // };

  // addValue = () => {
  //   if (this.state.inputValue) {
  //     this.setState({
  //       ...this.state,
  //       items: this.state.items.concat(this.state.inputValue)
  //     });
  //   };
  //   console.log(this.state.items);
  // };

  // deletedItem = (id) => {
  //   this.setState({
  //     items: this.state.items.splice(id, 1)
  //   });
  // };

  render() {
    console.log('App', this.props)
    return (
      <div style={{
        width: 500,
        margin: 100,
      }}>
        <input type='text' onChange={this.props.onChangeValue} />
        <button onClick={this.props.addItem}>ADD </button>
        {this.props.items.map((item, index) => {
          return (
            <Items
              key={index}
              itemName={this.props.inputValue}
              deletedItem={() => this.props.deleted(index)}
            />
          )
        })}

      </div>
    )
  }
}
function mapStateToProps(state) {
  return state
};

function mapDispatchToProps(dispatch) {
  return {
    onChangeValue: (event) => dispatch({ type: 'CHANGE', payload: event.target.value }),
    addItem: () => dispatch({ type: 'ADD', payload: this.props.inputValue }),
    deleted: () => dispatch({ type: 'REMOVE' })
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(App)
const initialState = {
    inputValue: '',
    items: []
}
console.log(initialState.items)
export default function rootReducer(state = initialState, action) {
    switch (action.type) {
        case 'CHANGE':
            return {
                inputValue:action.payload
            }
        case 'ADD':
            return {
                items: state.items.concat(action.payload)
            }
    }
    return state
}
import React from 'react';

export const Items = props => {
    return (
        <div  style={{
            width:300,
            display:'flex',
            justifyContent:'space-between',
        }}>
            {props.itemName}
            <button onClick={props.deletedItem}>X</button>
        </div>
    )
}
